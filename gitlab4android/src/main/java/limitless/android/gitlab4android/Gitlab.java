package limitless.android.gitlab4android;

import limitless.android.gitlab4android.Api.Services.Authorization;

public class Gitlab {

    public static String baseUrl = "https://gitlab.com/";

    public Gitlab(){

    }

    public Authorization authorization(){
        return new Authorization(baseUrl);
    }


}
