package limitless.android.gitlab4android.Api.Endpoints;

import androidx.annotation.NonNull;
import limitless.android.gitlab4android.Model.Token;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface AuthorizationEndpoints {

    /**
     * @param clientId
     * @param clientSecret
     * @param code
     * @param grantType  default  : authorization_code
     * @param redirectUri
     * @return {@link Token}
     */
    @POST("/oauth/token")
    Call<Token> getToken(
            @Query("client_id") String clientId,
            @Query("client_secret") String clientSecret,
            @Query("code") String code,
            @Query("grant_type") String grantType,
            @Query("redirect_uri") String redirectUri
    );

}
