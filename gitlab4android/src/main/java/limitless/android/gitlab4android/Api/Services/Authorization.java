package limitless.android.gitlab4android.Api.Services;

import androidx.annotation.*;
import limitless.android.gitlab4android.Api.Endpoints.AuthorizationEndpoints;
import limitless.android.gitlab4android.Listeners.Listener;
import limitless.android.gitlab4android.Model.Token;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

import android.webkit.WebView;

public class Authorization {

    private String baseUrl;
    private AuthorizationEndpoints authorizationService;

    public Authorization(@NonNull String baseUrl){
        if (! baseUrl.endsWith("/"))
            throw new RuntimeException("Base url must be end with '\\'");
        this.baseUrl = baseUrl;
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.authorizationService = retrofit.create(AuthorizationEndpoints.class);
    }

    /**
     * Load this url in {@link WebView} and check returned result.
     * @param clientId --> app id, in your Gitlab > setting > applications > your application
     * @param redirectUri --> your returned callback in your Gitlab > setting > applications > your application
     * @param state
     * @param responseType
     * @param scopes
     * @return
     */
    public String authorizeUrl(@NonNull String clientId, @NonNull String redirectUri, @Nullable String state, @Nullable String responseType, @Nullable String scopes){
        if (responseType == null)
            responseType = "code";
        return baseUrl + "oauth/authorize?client_id=" + clientId + "&redirect_uri=" + redirectUri +"&response_type=" + responseType + "&state=" + state + "&scope=" + scopes;
    }

    public void getToken(
            @NonNull String clientId,
            @NonNull String clientSecret,
            @NonNull String code,
            @Nullable String grantType,
            @NonNull String redirectUri,
            @NonNull final Listener<Token> tokenListener){
        authorizationService.getToken(clientId, clientSecret, code, grantType, redirectUri).enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                if (response.isSuccessful()){
                    tokenListener.data(response.body());
                }else {
                    tokenListener.error(response.message());
                }
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                tokenListener.error(t.getMessage());
            }
        });
    }

}
