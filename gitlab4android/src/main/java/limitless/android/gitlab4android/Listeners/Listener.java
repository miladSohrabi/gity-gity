package limitless.android.gitlab4android.Listeners;

public interface Listener<T> {

    void data(T t);

    void error(String msg);

}
