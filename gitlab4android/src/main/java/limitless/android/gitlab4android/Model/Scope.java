package limitless.android.gitlab4android.Model;

import androidx.annotation.NonNull;

public enum Scope {

    // Grants complete read/write access to the API, including all groups and projects, the container registry, and the package registry.
    API("api"),
    // Grants read-only access to the authenticated user's profile through the /user API endpoint, which includes username, public email, and full name. Also grants access to read-only API endpoints under /users.
    READ_USER("read_user"),
    // Grants read access to the API, including all groups and projects, the container registry, and the package registry.
    READ_API("read_api"),
    //Grants read-only access to repositories on private projects using Git-over-HTTP or the Repository Files API.
    READ_REPOSITORY("read_repository"),
    // Grants read-write access to repositories on private projects using Git-over-HTTP (not using the API).
    WRITE_REPOSITORY("write_repository"),
    // Write Registry
    WRITE_REGISTRY("write_registry"),
    //  Grants permission to perform API actions as any user in the system, when authenticated as an admin user.
    SUDO("sudo"),
    // Grants permission to authenticate with GitLab using OpenID Connect. Also gives read-only access to the user's profile and group memberships.
    OPENID("openid"),
    // Grants read-only access to the user's profile data using OpenID Connect.
    PROFILE("profile"),
    // Grants read-only access to the user's primary email address using OpenID Connect.
    EMAIL("email");


    private String scope;

    Scope(@NonNull String scope){
        this.scope = scope;
    }

    public String getScope() {
        return scope;
    }

    public static Scope[] getAll(){
        Scope[] sc = new Scope[10];
        sc[0] = Scope.API;
        sc[1] = Scope.READ_API;
        sc[2] = Scope.READ_REPOSITORY;
        sc[3] = Scope.WRITE_REPOSITORY;
        sc[4] = Scope.WRITE_REGISTRY;
        sc[5] = Scope.SUDO;
        sc[6] = Scope.OPENID;
        sc[7] = Scope.PROFILE;
        sc[8] = Scope.API;
        sc[9] = Scope.EMAIL;
        return sc;
    }

}
