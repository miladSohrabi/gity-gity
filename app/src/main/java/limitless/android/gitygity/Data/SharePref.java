package limitless.android.gitygity.Data;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import limitless.android.gitygity.Util.Constant;

public class SharePref {

    public static String isLogin = "isLogin";
    public static String accessToken = "accessToken";
    public static String refreshToken = "refreshToken";
    public static String tokenType = "tokenType";
    public static String tokenExpiresIn = "tokenExpiresIn";
    private SharedPreferences sp;

    public SharePref(Context context){
        sp = context.getSharedPreferences(Constant.sharePrefName, Context.MODE_PRIVATE);
    }

    public void put(String key, String value){
        sp.edit().putString(key, value).apply();
    }

    public String get(String key, String def){
        return sp.getString(key, def);
    }

    public void put(String key, boolean value){
        sp.edit().putBoolean(key, value).apply();
    }

    public boolean get(String key, boolean def){
        return sp.getBoolean(key, def);
    }

    public void put(String key, long value){
        sp.edit().putLong(key, value).apply();
    }

    public long get(String key, long def){
        return sp.getLong(key, def);
    }

}
