package limitless.android.gitygity.UI.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import limitless.android.gitygity.Data.SharePref;
import limitless.android.gitygity.Util.Tools;
import limitless.android.gitygity.databinding.ActivityMainBinding;

import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private final int login = 6456;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        init();
    }

    private void init() {
        if (! new SharePref(this).get(SharePref.isLogin, false)){
            startActivityForResult(new Intent(this, LoginActivity.class), login);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == login){
            if (resultCode == RESULT_OK){

            }else {
                Tools.toast(this, "Login failed");
                finish();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
