package limitless.android.gitygity.UI.Activity;

import androidx.appcompat.app.AppCompatActivity;
import limitless.android.gitlab4android.Gitlab;
import limitless.android.gitlab4android.Listeners.Listener;
import limitless.android.gitlab4android.Model.Scope;
import limitless.android.gitlab4android.Model.Token;
import limitless.android.gitygity.Data.SharePref;
import limitless.android.gitygity.R;
import limitless.android.gitygity.Util.Tools;
import limitless.android.gitygity.databinding.ActivityLoginBinding;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class LoginActivity extends AppCompatActivity {

    private ActivityLoginBinding binding;
    private WebChromeClient webChromeClient = new WebChromeClient(){
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            binding.progressBar.setProgress(newProgress);
        }
    };
    private WebViewClient webViewClient = new WebViewClient(){
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.contains(getString(R.string.redirect_uri))){
                Uri uri = Uri.parse(url);
                new Gitlab().authorization().getToken(
                        getString(R.string.app_id),
                        getString(R.string.app_secret),
                        uri.getQueryParameter("code"),
                        "authorization_code",
                        getString(R.string.redirect_uri),
                        new Listener<Token>() {
                            @Override
                            public void data(Token token) {
                                SharePref sharePref = new SharePref(LoginActivity.this);
                                sharePref.put(SharePref.accessToken, token.getAccessToken());
                                sharePref.put(SharePref.refreshToken, token.getRefreshToken());
                                sharePref.put(SharePref.tokenType, token.getTokenType());
                                sharePref.put(SharePref.tokenExpiresIn, token.getExpiresIn());
                                sharePref.put(SharePref.isLogin, true);
                                setResult(RESULT_OK);
                                finish();
                            }

                            @Override
                            public void error(String msg) {
                                Tools.toast(LoginActivity.this, msg);
                            }
                        }
                );
            }
            binding.webView.loadUrl(url);
            return true;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        init();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void init() {
        binding.webView.setWebViewClient(webViewClient);
        binding.webView.setWebChromeClient(webChromeClient);
        binding.webView.getSettings().setJavaScriptEnabled(true);

        StringBuilder sb = new StringBuilder();
        for (Scope s : Scope.getAll()){
            sb.append(s.getScope()).append("+");
        }
        String url = new Gitlab()
                .authorization()
                .authorizeUrl(
                        getString(R.string.app_id),
                        getString(R.string.redirect_uri),
                        null,
                        "code",
                        sb.toString().substring(sb.toString().length() - 1));
        binding.webView.loadUrl(url);

    }
}
