package limitless.android.gitygity.Util;

import android.content.Context;
import android.widget.Toast;

public class Tools {
    public static void toast(Context c, String s) {
        if (c == null || s == null)
            return;
        Toast.makeText(c, s, Toast.LENGTH_SHORT).show();
    }
}
